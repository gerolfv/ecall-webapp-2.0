var iv;
// the auto-refresh interval
var interval=5000;
var activeel;
var lasttitel="";

$(document).ready(function(){
    // add nice functionality to String
    if (typeof String.prototype.startsWith != 'function') {
       String.prototype.startsWith = function(str) {
        return this.slice(0,str.length) == str;
       };
    }
    // language switcher
    jQuery('body').click(function(){
        $('.language-switch ul').hide();
    });

    $('.language-switch .trigger').click(function(e){
        $('.language-switch ul').toggle();
        e.stopPropagation();
    });
    // enable auto-refresh if we are not in 'fixed mode' (fixed Ecall URL)
    if(!window.location.pathname.startsWith('/showIncident'))
    {
        // the incident list
        getIncidentList();
        setupRefresh();
    }
    // maximize the window
    use_big_layout();
    // clear search indication
    clearSearchIndication();

    setupClipboard();
});

function fix_block_clicks()
{
    jQuery('.message-box').click(function(){
       fetch_block_content(jQuery(this));
    });
}

function fix_block_nav_clicks()
{
    jQuery('.content-links a').click(function(){
       show_block_content(jQuery(this));
    });
}

function callback() {
    alert("SHOULD SHOW TELEPHONE POPUP");
}

function resendMSD() {
    var data = 'bogus';
    jsRoutes.controllers.Application.resendMSD().ajax
        ({
            data: data,
            type: "GET",
            timeout: 3000,
            success: function(html)
            {
                alert("resend asked");
            },
            error: function()
            {
               $('.messages').html('Er is iets misgelopen.');
            }
        });
}

function fetch_block_content(el)
{
    $('.message-box').removeClass('active');
    var data = el.attr('data-block-id');
    // remember for auto-refresh
    activeel = el.attr('data-block-id');
    jsRoutes.controllers.Application.incidentDetail(data).ajax
    ({
        timeout: 5000,
        success: function(html)
        {
           $('#block-content').html(html);
           el.addClass('active');
           use_big_layout();
           fix_block_nav_clicks()
           // get the map
           loadImage(data)
           //setTimeout("loadImage('ecall'+data+'_low.png')",2000);
        },
        error: function()
        {
           $('#block-content').html('...');
        }
    });

    /*
    var pageId = el.attr('data-block-id');
    $.ajax({
       url: 'ajax/'+pageId+'.html?ts='+(new Date().getTime()),
       dataType: 'html',
       success: function(data)
       {
           $('#block-content').html(data);
           el.addClass('active');
           use_big_layout();
           fix_block_nav_clicks()
       },
       error: function()
       {
           $('#block-content').html('Er is iets misgelopen.');
       }
    });
    */
}

function show_block_content(el)
{
    jQuery('.content-links a').removeClass('active');
    var contentBlockId = el.attr('data-target');
    jQuery('.content-body > div').hide();
    jQuery('#'+contentBlockId).show();
    el.addClass('active');
}

function use_small_layout()
{
    jQuery('#content').hide();
    jQuery('#maximize-container').show();
    try {
        top.window.resizeTo(530,1024);
    } catch (e) {
        //access denieds may happen here (IE bug?). Stay silent...
    }
}

function use_big_layout()
{
    jQuery('#content').show();
    jQuery('#maximize-container').hide();
    try {
        top.window.resizeTo(1280,1024);
    } catch (e) {
        //access denieds may happen here (IE bug?). Stay silent...
    }
}

function setupClipboard(){
  new Clipboard('.clipboard');
}

function copyToClipboard(text)
{
  console.log("function called with: " + text);
  console.log(document.execCommand('copy',false, text));
}

function setupUrlData(id)
{
    var url = "http://"+window.location.host+"/showIncident/"+id;
    jQuery("#copyURLicon").attr("data-clipboard-text", url);
}

function setupVinData(vin)
{
  jQuery("#copyVinLink").attr("data-clipboard-text", vin);
}

function setupLocationData(latitude,longitude)
{
    //alert('LL2('+longitude.trim()+','+latitude.trim()+')');
    //copyToClipboard('LL2('+latitude+' '+longitude+')');
    // Jan Zeinstra's request (mail from 15/11)
    var location = 'LL2('+longitude+','+latitude+')';
    jQuery("#copyLocLink").attr("data-clipboard-text", location);
    //create_growl('VIN copied to clipboard', 'success');
}

// ***********************************

function getIncidentList()
{
    //$('.messages').html('<img src="/assets/images/ajax-loader.gif"/>')
    var data = $('#timeStamp').serialize();
    jsRoutes.controllers.Application.incidentList().ajax
    ({
        data: data,
        type: "GET",
        timeout: 3000,
        success: function(html)
        {
          if(iv)
          {
              $('.messages').html(html);
              // block clicks
              fix_block_clicks();
              // make sure a currently selected block is styled active
              $('div[data-block-id='+activeel+']').addClass('active');
              // clear search indication
              clearSearchIndication();
          }
        },
        error: function()
        {
           $('.messages').html('Er is iets misgelopen.');
        }
    });
    // set the last timestamp (timestamp of the last refresh)
    $('#timeStamp').val(new Date().getTime());
}


function setSearchIndication()
{
   $('.search-box').addClass('searchMode');
   lasttitel=jQuery('#titeltext').val();
   jQuery('#titeltext').html(searchtitel);
}

function clearSearchIndication()
{
   $('.search-box').removeClass('searchMode');
   jQuery('#titeltext').html(lasttitel);
}


function incidentSearch()
{
    // check whether the searchfield is empty, in which case the user really just wants to clear the filter
    if ($("input[name='query']").val() == '') {
        getIncidentList();
        // clearing the filter start auto-refresh again
        setupRefresh();
        clearSearchIndication();
        return;
    }
    setSearchIndication();
    // disable auto-reload
    stopRefresh();

    $('.messages').html('<img src="/assets/images/ajax-loader.gif"/>')
    var data = $('.search-box').serialize();
    jsRoutes.controllers.Application.incidentSearch().ajax
    ({
        data: data,
        type: "GET",
        timeout: 3000,
        success: function(html)
        {
           $('.messages').html(html);
           // block clicks
           fix_block_clicks();
        },
        error: function()
        {
           $('.messages').html('...');
        }
    });
}

function setupRefresh() {
  //iv = setInterval("getIncidentList();", interval,interval);
  iv = setInterval(getIncidentList, interval);
}

function stopRefresh() {
   clearInterval(iv);
   // FIX 21/2/18 search "disappears after few seconds"
   iv=0;
}

function clearSearch() {
   $("input[name='query']").val("")
}

function changeLocale(locale) {
  var data = locale.serialize;
  jsRoutes.controllers.Application.changeLocale(locale).ajax
  ({
    success: function()
    {
     // refresh
     location.reload(forceGet=true);
    },
    error: function()
    {
      $('.messages').html('Er is iets misgelopen.');
    }
  });
}

function loadImage(ecallId) {
   jsRoutes.controllers.GoogleMapImages.imagesReady(ecallId).ajax
      ({
          type: "GET",
          timeout: 6000,
          success: function(html)
          {
                if(html!="nok")
                {
                    //$('#map-container').html('<img id="map" src="/assets/images/maps/ecall'+ecallId+'_mid.png" />');
                    //$('#thumb1').html('<img src="/assets/images/maps/ecall'+ecallId+'_low.png"/>');
                    //$('#thumb2').html('<img id="map" src="/assets/images/maps/ecall'+ecallId+'_mid.png"/>');
                    //$('#thumb2').addClass('active');
                    //$('#thumb3').html('<img id="map" src="/assets/images/maps/ecall'+ecallId+'_high.png"/>');
                    setTimeout(function() {
                        $('#map-container').html('<img id="map" src="/map/ecall'+ecallId+'_mid.png" />');
                        $('#thumb1').html('<img src="/map/ecall'+ecallId+'_low.png"/>');
                        $('#thumb2').html('<img id="map" src="/map/ecall'+ecallId+'_mid.png"/>');
                        $('#thumb2').addClass('active');
                        $('#thumb3').html('<img id="map" src="/map/ecall'+ecallId+'_high.png"/>');
                    }, 250);
                }
                else
                {
                    // try again
                    //setTimeout(loadImage(ecallId),250);
                }
          },
          error: function(x, t, m) {
                  if(t==="timeout") {
                      alert("got timeout while waiting for Google Maps");
                  } else {
                      alert(t);
                  }
              }
      });
}

function changeImage(nr) {
    var src = $('#thumb'+nr+' img').attr('src');
    $('.thumb-container a').removeClass('active');
    //TODO apply this to the 1st image only
    $('#map-container img').attr('src',src);
    $('#thumb'+nr).addClass('active');
}


function create_growl(message, type) {
  if (!jQuery('#growl_container').length) {
    jQuery('<div id="growl_container"></div>').appendTo(jQuery('body'));
  }

  type = typeof type == 'undefined' ? 'normal' : type;
  var id = 'growl_'+Math.floor(Math.random()*100000);
  var msg = jQuery('<div class="growl growl-'+type+'" id="'+id+'">'+message+'</div>');
  msg.click(function() { jQuery(this).hide(); });
  msg.appendTo(jQuery('#growl_container'));
  setTimeout(function() {jQuery('#'+id).fadeOut('slow'); }, 3000);
}


name := """ecall-webapp2.0"""
organization := "be.astrid"

version := "1.7"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += "org.slf4s" %% "slf4s-api" % "1.7.25"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"
libraryDependencies += "com.jolbox" % "bonecp" % "0.7.1.RELEASE"
libraryDependencies += "net.sourceforge.jtds" % "jtds" % "1.2"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.10"
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "be.astrid.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "be.astrid.binders._"

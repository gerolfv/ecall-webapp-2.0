general.error = Er is een fout opgetreden
detail.notAvailable = Geen detail beschikbaar voor Ecall met id{0}

# interface labels
interface.cleardown = Afgehandeld
inerface.callback = Bel me terug
interface.resendMSD = retransmit MSD
interface.copyUrl = URL kopieren
interface.copyVIN = VIN kopieren
interface.copyLocation = Locatie kopieren for I/Dispatcher
interface.lastmessages = Laatste berichten
interface.small = Inklappen
interface.large = Uitklappen
interface.help = Hulp
interface.selectBlock = Selecteer een incident links om het detail ervan te bekijken
interface.search = SID, UID of een gedeelte ervan
interface.searchResults = Zoekresultaten

# ecall labels
ecall.received=Ontvangen

# MSD labels
msd.activation = Activatie
msd.vehicleid = Chassisnummer
msd.propulsion = Brandstoftype
msd.timestamp = Tijd
msd.location = Locatie
msd.direction = Richting
msd.passengers = Inzittenden
msd.vehicletype = Voertuigtype
msd.vehiclephone = Voertuigtelefoon
msd.positiontrust = Betrouwbaar?
msd.positiontrustable = Ja
msd.positionnottrustable = Neen

# MSD value translations
uid = Unieke ID
sid = Evenement code
passengerVehicleClassM1 = personenwagen (categorie M1)
busesAndCoachesClassM2 = autobussen en autocars (categorie M2)
busesAndCoachesClassM3 = autobussen en autocars (categorie M3)
lightCommercialVehiclesClassN1 = lichte bedrijfsvoertuigen (categorie N1)
heavyDutyVehiclesClassN2 = zware bedrijfsvoertuigen (categorie N2)
heavyDutyVehiclesClassN3 = zware bedrijfsvoertuigen (categorie N3)
motorcyclesClassL1e = motorfietsen (categorie L1e)
motorcyclesClassL2e = motorfietsen (categorie L2e)
motorcyclesClassL3e = motorfietsen (categorie L3e)
motorcyclesClassL4e = motorfietsen (categorie L4e)
motorcyclesClassL5e = motorfietsen (categorie L5e)
motorcyclesClassL6e = motorfietsen (categorie L6e)
motorcyclesClassL7e = motorfietsen (categorie L7e)

gasoline = benzinetank
diesel = dieseltank
compressednaturalgas = aardgas onder druk (CNG)
liquidpropanegas = vloeibaar gemaakte petroleumgassen (LPG)
electric = opslag van elektrische energie (met meer dan 42 V en 100 Ah)
hydrogen = waterstofopslag

# additional data
additional.noadditional = Geen extra info beschikbaar

# Raw data
raw.nodata = XML data niet beschikbaar

# the maps
googlemaps.lowzoom = overzichtsbeeld
googlemaps.midzoom = normaal beeld
googlemaps.highzoom = detailbeeld
compass = compas

# the extended functionality (resend MSD, callback, ...)
extended.msdupdated = MSD vernieuwd



# help
help.title = Ecall Webapp help
help.title.general = Algemeen
help.title.list = De Incidentlijst
help.title.detail = Het detail
help.title.search = Incidenten zoeken
help.title.msd = MSD
help.title.additional = Extra data
help.title.raw = De XML bron

help.text.general = De Ecall Web applicatie laat toe om E-call incidenten snel en efficient te raadplegen. De interface is opgebouwd naar analogie met een email programma (zoals bvb. Mozilla Thunderbird, Microsoft Outlook, e.a.), met een overzichtslijst links en een detail rechts op het scherm. Beide elementen worden hieronder in meer detail behandelt.  Het detail kan 'ingeklapt' worden, zodanig dat enkel de overzichtslijst op een compacte manier op het scherm getoond kan worden. Bovenaan rechts vinden we vervolgens nog de knop om deze help-pagina te openen en de taalkeuze. De taalkeuze wordt bewaard op uw computer in een cookie.
help.text.list = Het linkergedeelte van de interface bestaat uit de incidentenlijst. Deze lijst bevat alle incidenten van de laatste {0} minuten, gesorteerd volgens tijdstip met het meest recente incident bovenaan. Deze lijst ververst automatisch elke 5 seconden, maar kan ook manueel vernieuwd worden door op het draaiend pijltje bovenaan te klikken. Bij het klikken op een incident wordt het detail getoond in het rechtergedeelte van het scherm. Als het detail 'ingeklapt' is zal het automatisch openen wanneer een incident uit de lijst aangeklikt wordt.
help.text.search = Boven de incidentenlijst vindt u het zoekveld. Daarin kan u de unieke ID (UID) of 'short ID' (SID) van een Ecall bericht typen om het bericht op te zoeken. U kan ook een gedeelte van de ID invullen, en de applicatie zal alle berichten die de ingevulde term in hun UID of SID hebben als resultaat tonen. Met behulp van de wildcard '%' kan u de zoekactie in detail sturen. Een '%' voor de term betekent dat u zoekt op ID's die <strong>eindigen</strong> met die term. Een '%' na de term betekent dat u zoekt op ID's die <strong>beginnen</strong> met die term. U kan de '%' ook in het midden van de zoekterm plaatsen, om te zoeken op ID's die beginnen met het eerste en eindigen met het tweede deel van de term. Het resultaat van de zoekactie wordt in de lijst getoond. Opgelet: Deze bevat op dat moment dus niet meer de recentste incidenten, maar de resultaten van de zoekactie. Door het zoekveld terug leeg te maken, of door op het 'ververs' icoon te drukken wordt de zoekfilter geannuleerd en ziet u opnieuw de meest recente berichten.
help.text.detail = Het rechtergedeelte van de interface toont het detail van het geselecteerde E-call. Een E-call bericht bestaat gewoonlijk uit 2 delen: een 'Minimal Set of Data' (MSD) een een sectie mt extra data ('additional data'). De applicatie toont deze 2 secties op overzichtelijk wijze in de eerste 2 tab-bladeren van het detailscherm. Onder het 3de tab-blad vindt u de originele E-call XML.
help.text.msd = Het MSD gedeelte bevat de belangrijkste informatie over het incident. Behalve de IDs, het tijdstip en het type bericht (automatisch?, testbericht?) bevat de MSD voertuiginfo (chassisnr, brandstoftype, locatie) en telefoonnrs voor de TPSP die het bericht stuurde en het voertuig zelf (indien aanwezig). De voertuiglocatie wordt tevens getoond op een kaart, beschikbaar in 3 verschillende zoomlevels.
help.text.additional = In dit gedeelte komt alle extra data die de Third Party centrale stuurt terecht. Let op: niet alle E-call berichten bevatten extra data. In dat geval zal dit tab-blad leg zijn. Wanneer er wel extra data aanwezig is worden de waarden in een niet-hierarchische 'key-value' manier getoond, aangezien de betekenis van deze data niet vast staat en per TPSP en E-call bericht kan verschillen.
help.text.raw = Het detail gedeelte van de E-call web applicatie heeft een 3de tab-blad waarop het originele XML bericht wordt getoond. Dit enkel om bij twijfel te kunnen nakijken wat er origineel in het bericht stond. Verder is dit tab-blad van weinig belang.
general.error = Une erreur s’est produite
detail.notAvailable = Aucun détail disponible pour l’Ecall avec l’ID {0}

# interface labels
interface.cleardown = Message traité
inerface.callback = Rappeler
interface.resendMSD = Retransmettre MSD
interface.copyUrl = Copier l’URL
interface.copyVIN = Copier le VIN
interface.copyLocation = Copier la position pour I/Dispatcher
interface.lastmessages = Derniers messages
interface.small = Refermer
interface.large = Ouvrir
interface.help = Aide
interface.selectBlock = Cliquez sur un incident pour en voir les détails
interface.search = SID, UID ou partie d’une ID
interface.searchResults = Résultats de recherche

# ecall labels
ecall.received = Reçu

# MSD labels
msd.activation = Activation
msd.vehicleid = Numéro de chassis
msd.propulsion = Type de carburant
msd.timestamp = Date et heure
msd.location = Position
msd.direction = Direction
msd.passengers = Occupants
msd.vehicletype = Type de véhicule
msd.vehiclephone = Téléphone
msd.positiontrust = Fiable ?
msd.positiontrustable = Oui
msd.positionnottrustable = Non

# MSD value translations
uid = ID Unique
sid = Code d'événement
passengerVehicleClassM1 = véhicule de voyageurs (catégorie M1)
busesAndCoachesClassM2 = autobus et autocars (catégorie M2)
busesAndCoachesClassM3 = autobus et autocars (catégorie M3)
lightCommercialVehiclesClassN1 = véhicules utilitaires légers (catégorie N1)
heavyDutyVehiclesClassN2 = véhicules utilitaires lourds (catégorie N2)
heavyDutyVehiclesClassN3 = véhicules utilitaires lourds (catégorie N3)
motorcyclesClassL1e = motocycles (catégorie L1e)
motorcyclesClassL2e = motocycles (catégorie L2e)
motorcyclesClassL3e = motocycles (catégorie L3e)
motorcyclesClassL4e = motocycles (catégorie L4e)
motorcyclesClassL5e = motocycles (catégorie L5e)
motorcyclesClassL6e = motocycles (catégorie L6e)
motorcyclesClassL7e = motocycles (catégorie L7e)

gasoline = réservoir d'essence
diesel = réservoir de diesel
compressednaturalgas = gaz naturel comprimé (GNC)
liquidpropanegas = gaz propane liquide (GPL)
electric = accumulateur d'énergie électrique (avec plus de 42 V et 100 Ah)
hydrogen = accumulateur d'hydrogène

# additional data
additional.noadditional = Aucune info complémentaire disponible

# Raw data
raw.nodata = Données XML indisponibles

# the maps
googlemaps.lowzoom = Vue d’ensemble
googlemaps.midzoom = Vue standard
googlemaps.highzoom = Vue détaillée
compass = compas

# Detail bericht tabbladden
#titel “Msd” = MSD
#titel “Additional” = Compléments
#titel “Raw XML” = Fichier XML

# the extended functionality (resend MSD, callback, ...)
extended.msdupdated = MSD actualisé

package services

import com.typesafe.config.Config
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.Injecting
import util.{GoogleMapDownloader, LocationInfo}

import scala.concurrent.ExecutionContext

class GoogleMapTest extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "GoogleMapDownloader" should {

    "download with apikey" in {

      //val gmd = new GoogleMapDownloader(Config, ExecutionContext.global)
      //gmd.generateMapsAsync("1",generatePositions)
    }

  }

  def generatePositions = {
    LocationInfo(Seq(Position(50.805745, 4.131557,192),Position(50.809975, 4.133016,195),Position(50.815195, 4.128326,100)))
  }

}

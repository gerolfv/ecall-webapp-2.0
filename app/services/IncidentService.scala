
package services


import com.google.inject.Inject
import org.joda.time.DateTime
import org.slf4s.Logging
import util.ConnectionPool
import util.Control._

import scala.collection.mutable

/**
 * Created by: gerolf
 * Date: 09/01/14
 * Time: 16:38
 */

case class Incident(id:Int,date:DateTime,uuid:String,sid:String,telephone:String,reqtype:String,tpsp:String)

class IncidentService @Inject()(cp:ConnectionPool) extends Logging {


  def recentEcalls(maxAge:Long) : Option[List[Incident]] = {
    val rv = mutable.ListBuffer.empty[Incident]
    try {
      using(cp.getConnection) {con =>
        using(con.prepareStatement("SELECT e.id,e.uuid,e.sid,m.automaticactivation,m.testcall,e.arrivaltime,e.tpspname,e.vehiclePhone  FROM Ecall e INNER JOIN Msd m ON m.ecall_fk=e.id WHERE e.arrivaltime > ? ORDER BY e.arrivaltime DESC")) {st =>
          st.setLong(1,maxAge)
          using(st.executeQuery) {rs =>
            while(rs.next()) {
              val id=rs.getInt("id")
              val uuid=rs.getString("uuid")
              val sid=rs.getString("sid")
              val timestamp=rs.getLong("arrivaltime")
              val auto = rs.getBoolean("automaticactivation")
              val test = rs.getBoolean("testcall")
              val tpsp = rs.getString("tpspname")
              val vehiclePhone = rs.getString("vehiclePhone")
              val reqtype = auto match {
                case true if test => "automatic / test"
                case false if test => "manual / test"
                case true if !test => "automatic"
                case false if! test => "manual"
              }
              rv append Incident(id,DateTime.now.withMillis(timestamp),uuid,sid,vehiclePhone,reqtype,tpsp)
            }
          }
        }
      }
      Some(rv.toList)
    }
    catch {
      case e: Throwable => {
        log.error("error while retrieving recent ecall list",e)
        None // return none on exception
      }
    }
  }

  def searchForEcalls(term:String) : Option[List[Incident]] = {
    try {
      var value1=""
      var value2=""
      var wherecase=0
      val where = term.count(_ == '%') match {
        case 1 if (term.indexOf('%')==0) || (term.indexOf('%')==term.size-1) => {
          value1=term
          value2=term
          wherecase=1
          "e.uuid LIKE ? OR e.sid LIKE ?"
        }
        case 1 if (term.split('%')(0).size > 0) && (term.split('%')(1).size > 0) => {
          val split = term.split('%')
          value1 = split(0)
          value2 = split(1)
          wherecase=2
          "(e.uuid LIKE ? AND e.uuid LIKE ?) OR (e.sid LIKE ? AND e.sid LIKE ?)"
        }
        case 0 => {
          value1=term
          value2=term
          wherecase=3
          "e.uuid LIKE ? OR e.sid LIKE ?"
        }
        case _ => {
          value1=term
          value2=term
          wherecase=4
          "e.uuid LIKE ? OR e.sid LIKE ?"
        }
      }
      // get the data
      val rv = mutable.ListBuffer.empty[Incident]
      using(cp.getConnection) { con =>

        using(con.prepareStatement("SELECT e.id,e.uuid,e.sid,m.automaticactivation,m.testcall,m.timestamp,e.tpspname,e.vehiclePhone FROM Ecall e INNER JOIN Msd m ON m.ecall_fk=e.id WHERE "+where+" ORDER BY m.timestamp DESC")) {st =>
          wherecase match {
            case 1 => {
              st.setString(1,value1)
              st.setString(2,value2)
            }
            case 2 => {
              st.setString(1,value1+"%")
              st.setString(2,"%"+value2)
              st.setString(3,value1+"%")
              st.setString(4,"%"+value2)
            }
            case 3 => {
              st.setString(1,"%"+value1+"%")
              st.setString(2,"%"+value2+"%")
            }
            case 4 => {
              st.setString(1,value1)
              st.setString(2,value2)
            }
          }

          using(st.executeQuery) {rs =>
            while(rs.next()) {
              val id=rs.getInt("id")
              val uuid=rs.getString("uuid")
              val sid=rs.getString("sid")
              val timestamp=rs.getLong("timestamp")
              val auto = rs.getBoolean("automaticactivation")
              val test = rs.getBoolean("testcall")
              val tpsp = rs.getString("tpspname")
              val vehiclePhone = rs.getString("vehiclePhone")
              val reqtype = auto match {
                case true if test => "automatic / test"
                case false if test => "manual / test"
                case true if !test => "automatic"
                case false if! test => "manual"
              }
              rv append  (Incident(id,DateTime.now.withMillis(timestamp),uuid,sid,vehiclePhone,reqtype,tpsp))
            }
          }
        }
      }
      Some(rv.toList)
    } catch {
      case e: Throwable => {
        log.error("error while searching for ecalls with term %s" format term,e)
        None // return none on exception
      }
    }
  }
}

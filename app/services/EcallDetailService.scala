package services

import com.google.inject.Inject
import util.Control._

import scala.Some
import util.ConnectionPool
import org.joda.time.DateTime

import scala.collection.mutable

/**
 * Created by: gerolf
 * Date: 09/01/14
 * Time: 16:17
 */
class EcallDetailService @Inject()(cp:ConnectionPool){


  def getMsd(ecallid:Int):Option[MSD] = {
    NoneOnException {
      using(cp.getConnection) { con =>
        using(con.prepareStatement("SELECT * FROM Ecall e INNER JOIN Msd m ON m.ecall_fk=e.id WHERE e.id=?")) {st =>
          st.setInt(1,ecallid)
          using(st.executeQuery) {rs =>
            singleResult(rs.next()) {
              val uuid=rs.getString("uuid")
              val sid=rs.getString("sid")
              val timestamp=rs.getLong("timestamp")
              val auto = rs.getBoolean("automaticactivation")
              val test = rs.getBoolean("testcall")
              val tpsp = rs.getString("tpspname")
              val msdversion = rs.getInt("msdversion")
              val messageid = rs.getInt("messageid")
              val vehiclephone = rs.getString("vehiclephone")
              val tpspphone = rs.getString("tpspphone")
              val vehicletype = rs.getString("vehicletype")
              val propulsion = rs.getString("propulsionstorage")
              val positiontrust = rs.getBoolean("positiontrustable")
              val nrpass = rs.getInt("nrpassengers")
              val wmi = rs.getString("wmi")
              val vds = rs.getString("vds")
              val vismy = rs.getString("vismodelyear")
              val visplant = rs.getString("visseqplant")
              val oid = rs.getString("oid")
              val data = rs.getString("data")
              val lat0 = rs.getDouble("vehicle_latitude")
              val long0 = rs.getDouble("vehicle_longitude")
              val heading = rs.getInt("vehicle_direction")
              val lat1 = rs.getDouble("recent_latitude_1")
              val long1 = rs.getDouble("recent_longitude_1")
              val lat2 = rs.getDouble("recent_latitude_2")
              val long2 = rs.getDouble("recent_longitude_2")
              val pos = Seq(Position(lat0,long0,heading),Position(lat1,long1,heading),Position(lat2,long2,heading))
              Some(MSD( uuid,sid,tpsp,vehiclephone,tpspphone,msdversion,
                messageid,auto,test,positiontrust,vehicletype,wmi,
                vds,vismy,visplant,propulsion,DateTime.now.withMillis(timestamp),
                pos,nrpass,oid,data))
            }
          }
        }
      }
    }
  }

  def getAdditionalInfo(ecallid:Int):Option[Additional] = {
    NoneOnException {
      using(cp.getConnection) { con =>
        using(con.prepareStatement("SELECT * FROM ExtraInfo WHERE ecall_fk=? ORDER by id")) {st =>
          st.setInt(1,ecallid)
          val add = mutable.LinkedHashMap.empty[String,String]
          using(st.executeQuery) {rs =>
            while(rs.next()) {
              val k = rs.getString("item")
              val v = rs.getString("value")
              add += (k -> v)
            }
            Some(Additional(add))
          }
        }
      }
    }
  }

  def getRaw(ecallid:Int):Option[String] = {
    NoneOnException {
      using(cp.getConnection) { con =>
        using(con.prepareStatement("SELECT xml FROM Raw WHERE ecall_fk=?")) {st =>
          st.setInt(1,ecallid)
          using(st.executeQuery) {rs =>
            singleResult(rs.next()) {
              Some(rs.getString("xml"))
            }
          }
        }
      }
    }
  }

}

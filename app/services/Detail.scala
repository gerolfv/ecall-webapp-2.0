package services

import org.joda.time.DateTime
import play.api.i18n._
import play.api.libs.json.Json._
import util.Conversions._
import util.DateTimeConversions

import scala.collection.mutable
import scala.xml._

/**
 * Created by: gerolf
 * Date: 31/07/13
 * Time: 16:35
 */
case class Position(latitude:Double,longitude:Double,heading:Int) {
  override def toString = latitude+","+longitude
}

case class MSD( uuid:String,sid:String,tpspname:String, vehiclePhone:String, tpspPhone:String,
                msdversion:Int,messageid:Int,automatic:Boolean,test:Boolean,
                posisitiontrustable:Boolean,vehicletype:String,wmi:String,vds:String,
                vismodelyear:String,visseqplant:String,propulsion:String,timestamp:DateTime,
                positions:Seq[Position],nrpassengers:Int,oid:String,data:String)

case class Additional(kv: mutable.LinkedHashMap[String,String])
case object Additional {
  def empty = Additional(mutable.LinkedHashMap.empty[String,String])
}

case class JsonDetail(msd:MSD, add:Option[Additional], raw:Option[String])(implicit lang:Lang, messagesApi:MessagesApi)  extends DateTimeConversions{
  implicit val messagesProvider:MessagesProvider = MessagesImpl(lang,messagesApi)

  var content = {
     toJson(
        Map (
           "MSD" -> toJson(
             Map (
                "uuid" -> toJson(msd.uuid),
                "sid" -> toJson(msd.sid),
                "automaticActivation" -> toJson(msd.automatic),
                "test" -> toJson(msd.test),
                "vehicleId" -> toJson(
                  Map (
                    "wmi" -> toJson(msd.wmi),
                    "vds" -> toJson(msd.vds),
                    "modelyear" -> toJson(msd.vismodelyear),
                    "plant" -> toJson(msd.visseqplant)
                  )),
                "propulsion" -> toJson(msd.propulsion.split(" ").foldLeft("")((a:String,prop:String) => a +Messages(prop)+" ")),
                "timestamp" -> toJson(msd.timestamp),
                "location" -> toJson(Seq(
                  toJson(
                    Map(
                      "location" -> toJson(
                        Map (
                          "latitude" -> toJson(msd.positions(0).latitude),
                          "longitude" -> toJson(msd.positions(0).longitude)
                      )),
                      "recentLocation_1" -> toJson(
                        Map (
                          "latitude" -> toJson(msd.positions(1).latitude),
                          "longitude" -> toJson(msd.positions(1).longitude)
                      )),
                      "recentLocation_2" -> toJson(
                        Map (
                          "latitude" -> toJson(msd.positions(2).latitude),
                          "longitude" -> toJson(msd.positions(2).longitude)
                      ))
                    ))
                 )),
                "direction" -> toJson(msd.positions(0).heading*2),
                "passengers" -> toJson(msd.nrpassengers),
                "vehicleType" -> toJson(Messages(msd.vehicletype)),
                "vehiclePhone" -> toJson(msd.vehiclePhone),
                "tpsp" -> toJson(msd.tpspname),
                "tpspPhone" -> toJson(msd.tpspPhone)
             )
           ),
           "Additional" -> toJson(add.getOrElse(Additional.empty).kv.toMap map {case (k:String,v:String) => (k.trim,v.trim)})
        )
     )
    }
}


case class Detail(msd:MSD, add:Option[Additional], raw:Option[String])(implicit lang:Lang, messagesApi:MessagesApi) {
  implicit val messagesProvider:MessagesProvider = MessagesImpl(lang,messagesApi)

  val headingForCompass=msd.positions(0).heading*2 match {
    case 359 => 0
    case x:Int if(x%2==0) => x
    case x:Int => x+1
  }

  val content = {
      <div class="action-links">
        <a class="pull-left"  href="#" onClick='alert("TODO");return false;' title={Messages("interface.cleardown")}>{Messages("interface.cleardown")}</a>
        {<a id="copyURLicon" class="pull-right clipboard" href="#" title={Messages("interface.copyURL")} >
          {Messages("interface.copyUrl")}
        </a>}
        {<a id="resendMSD" onClick="resendMSD()" class="pull-right " href="#" title={Messages("interface.resendMSD")}>
          <img id="resendMSDIcon" src="/assets/images/resendMSD.jpg" alt="resend MSD icon" width="24" height="24" style="margin-right:10px;"/>
        </a>}
        {<a id="callback" onClick="callback()" class="pull-right " href="#" title={Messages("interface.callback")}>
          <img id="callbackIcon" src="/assets/images/callback.jpg" alt="callback icon"  width="24" height="24"  style="margin-right:10px;"/>
        </a>}
      </div>
      <div class="content-links">
        <a href="#" data-target="msd-block" title="Msd" class="active" >Msd</a>
        <a href="#" data-target="additional-block" title="Additional">Additional</a>
        <a href="#" data-target="xml-block" title="Raw XML">Raw XML</a>
      </div>
      <div class="content-body">
        <div id="msd-block">
          <div class="col">
            <ul>
              <li>
                <strong>{Messages("uid")}:</strong>
                {msd.uuid}
              </li>

              <li>
                <strong>{Messages("sid")}:</strong>
                {msd.sid}
              </li>

              <li>
                <strong>{Messages("msd.activation")}:</strong>
                {msd.automatic match {
                    case true if msd.test => "automatic / test"
                    case false if msd.test => "manual / test"
                    case true if !msd.test => "automatic"
                    case false if !msd.test => "manual"
                  }
                }
              </li>
              <li>
                <strong>{Messages("msd.vehicleid")}:</strong>
                {<a href="#" id="copyVinLink" class="clipboard" title={Messages("interface.copyVIN")}>
                  {<img id="copyVINicon" src="/assets/images/copy.png" alt="Copy icon" />% Attribute (None, "onload", Text("setupUrlData('"+msd.uuid+"');"+"setupVinData('"+msd.wmi.trim+msd.vds.trim+msd.vismodelyear.trim+msd.visseqplant.trim+"');"+ "setupLocationData('"+milliArcSecondsToDegrees(msd.positions(0).latitude)+"','"+milliArcSecondsToDegrees(msd.positions(0).longitude)+"');"), Null)}
                </a>}

                <ul>
                  <li>
                    <strong>ISO wmi:</strong>
                    {msd.wmi.trim}
                  </li>

                  <li>
                    <strong>ISO vds:</strong>
                    {msd.vds.trim}
                  </li>

                  <li>
                    <strong>modelyear:</strong>
                    {msd.vismodelyear.trim}
                  </li>

                  <li>
                    <strong>plant:</strong>
                    {msd.visseqplant.trim}
                  </li>

                </ul>
              </li>
              <li>
                <strong>{Messages("msd.propulsion")}:</strong>
                <span>
                {
                  msd.propulsion.split(" ").foldLeft("")((a:String,prop:String) => a + "  #"+Messages(prop))
                }
                </span>
              </li>
            </ul>
          </div>

          <div class="col">
            <ul>
              <li>
                <strong>{Messages("msd.timestamp")}:</strong>
                <span>{msd.timestamp.toString("dd/MM/yyyy HH:mm:ss")}</span>
              </li>

              <li>
                <strong>{Messages("msd.location")}:</strong>
                {<a href="#" id="copyLocLink" class="clipboard" title={Messages("interface.copyLocation") }>
                  <img id="copyLOCicon" src="/assets/images/copy.png" alt="Copy icon" />
                </a>}

                <ul>
                  <li>
                    <strong>Latitude:</strong>
                    {milliArcSecondsToDecimals(msd.positions(0).latitude)}
                  </li>

                  <li>
                    <strong>Longitude:</strong>
                    {milliArcSecondsToDecimals(msd.positions(0).longitude)}
                  </li>

                  <li>
                    <strong>{Messages("msd.positiontrust")}:</strong>
                    {msd.posisitiontrustable match {
                      case true => Messages("msd.positiontrustable")
                      case _ => Messages("msd.positionnottrustable")
                      }
                    }
                  </li>
                </ul>
              </li>

              <li>
                <strong>{Messages("msd.direction")}:</strong>
                {msd.positions(0).heading*2}
              </li>

              <li>
                <strong>{Messages("msd.passengers")}:</strong>
                {msd.nrpassengers}
              </li>
              <li>
                <strong>{Messages("msd.vehicletype")}:</strong>
                <span>{Messages(msd.vehicletype)}</span>
              </li>
              <li>
                <strong>{Messages("msd.vehiclephone")}:</strong>
                {msd.vehiclePhone}
              </li>
              <li>
                <strong>Tpsp:</strong>
                <span>{msd.tpspname+" / "+msd.tpspPhone}</span>
              </li>
            </ul>
          </div>

          <div class="clear-float"></div>
          {val mapclass = msd.posisitiontrustable match {
              case true => "locationTrustable"
              case _ => "locationNotTrustable"
            }
          }
          {<div id="map-container">

          </div> % Attribute (None, "class", Text(msd.posisitiontrustable match {
                                case true => "locationTrustable"
                                case _ => "locationNotTrustable"
                              }), Null)}

          <div class="thumb-container">
            <a id="thumb1" href="#" onClick="changeImage(1);return false;" title={Messages("googlemaps.lowzoom")}></a>
            <a id="thumb2" href="#" onClick="changeImage(2);return false;" title={Messages("googlemaps.midzoom")}></a>
            <a id="thumb3" href="#" onClick="changeImage(3);return false;" title={Messages("googlemaps.highzoom")}></a>
            <img id="direction_arrow" class="compass" href="#" src={"/assets/images/arrows/"+headingForCompass+".png"} title={Messages("compass")}></img>
          </div>
        </div>

        <div id="additional-block" style="display:none;">
          {add match {
            case Some(additional) => generateAdditionalPage(additional)(lang)
            case None => Messages("additional.noadditional")
            }
          }
        </div>
        <div id="xml-block" style="display:none;">
          <pre>
          {raw match {
            case Some(rawdata) => (new PrettyPrinter(80,2)).format(XML.loadString(rawdata))
            case None => Messages("raw.nodata")
            }
          }
          </pre>
        </div>
      </div>
  }

  def generateAdditionalPage(add:Additional)(implicit lang:Lang) = {
    val htmlbuilder = StringBuilder.newBuilder
    // lay out in 2 columns: calculate the nr of items in column 1:
    val itemscol1 = Math.floor(add.kv.size/2.0)
    htmlbuilder.append("<div><div class=\"widecol\"><ul>")
    add.kv foreach {case (k,v) =>
        htmlbuilder.append("<li><strong> %s </strong> %s </li>".format(k.trim,v.trim))
    }
    htmlbuilder.append("</ul></div></div>")
    XML.loadString(htmlbuilder.toString)
  }

}

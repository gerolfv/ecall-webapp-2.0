package services


import javax.inject._
import org.apache.http.{HttpStatus, NameValuePair}
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.{CloseableHttpResponse, HttpPost}
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import org.slf4s.Logging

import scala.collection.JavaConverters._
import scala.language.postfixOps
import scala.collection.mutable.ListBuffer
import util.AutoRefreshingProperties

/**
  * This trait contains methods to ask the XMLGateway (which is connected to the TPSPs)
  * for MSD updates or Callbacks. The handling of parsing any responses from TPSPs is
  * done by the XMLGateway
  * If all went well, the methods return true, indicating that the user may want to refresh for the
  * new data to appear
  */
//@Singleton
class IPCServices(URL:String ) extends Logging {

  // returns true if the call was placed successfully and the respoonse
  def askMSDUpdate(ecallId:Int):Boolean = {
    log.info("asking MSD update via IPC communication to XMLgateway")
    val workstation = 1 toString // TODO: how to deal with this? ask customer
    val uri =URL
    val post = new HttpPost(uri+"/resendMSD")
    val params = ListBuffer.empty[NameValuePair]
    params append new BasicNameValuePair("ecallId",ecallId toString)
    params append new BasicNameValuePair("workstation",workstation)
    post.setEntity(new UrlEncodedFormEntity(params.toList asJavaCollection))
    var response: CloseableHttpResponse = null
    try {
      log.info(s"asking NSD to url ${post}")
      response = HttpClients.createDefault().execute(post)
      if (response.getStatusLine.getStatusCode == HttpStatus.SC_OK) {
        var rv=false
        val entity = response.getEntity
        if(entity.toString.equals("ACK")) rv = true
        else rv = false
        // let's be nice and release underlying resources before returning
        EntityUtils.consume(entity)
        rv
      } else {
        log.warn(s"Got a ${response.getStatusLine.getStatusCode} HHTP code from the XMlgateway during IPC communication")
        false
      }
    } catch {
      case e:Exception => log.error("Error while doing IPC to xmlgateway",e)
        false
    } finally {
      if(response!=null) response.close()
    }
  }

  def askCallBack(ecallId:Int,wksphone:String):Boolean = {
    log.info("asking Callback via IPC communication to XMLgateway")
    val workstation = 1 toString // TODO: how to deal with this? ask customer
    val uri =URL
    val post = new HttpPost(uri+"/callback")
    val params = ListBuffer.empty[NameValuePair]
    params append new BasicNameValuePair("ecallId",ecallId toString)
    params append new BasicNameValuePair("workstation",workstation)
    params append new BasicNameValuePair("wksphone",wksphone)
    post.setEntity(new UrlEncodedFormEntity(params.toList asJavaCollection))
    var response: CloseableHttpResponse = null
    try {
      log.info(s"asking callback to url ${post}")
      response = HttpClients.createDefault().execute(post)
      if (response.getStatusLine.getStatusCode == HttpStatus.SC_OK) {
        var rv=false
        val entity = response.getEntity
        if(entity.toString.equals("ACK")) rv = true
        else rv = false
        // let's be nice and release underlying resources before returning
        EntityUtils.consume(entity)
        rv
      } else {
        log.warn(s"Got a ${response.getStatusLine.getStatusCode} HHTP code from the XMlgateway during IPC communication")
        false
      }
    } catch {
      case e:Exception => log.error("Error while doing IPC to xmlgateway",e)
        false
    } finally {
      if(response!=null) response.close()
    }
  }
}

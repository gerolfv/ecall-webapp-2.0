package controllers

import java.io.File

import akka.actor.ActorSystem
import com.google.inject.Inject
import org.joda.time.DateTime
import play.api.{Configuration, Logger}
import play.api.i18n.{I18nSupport, Langs, MessagesApi}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.{AbstractController, ControllerComponents}
import util.{AutoRefreshingProperties, IntValue, StringValue}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

/**
 * This controller's sole purpose is to give a response when the Google map images are ready to be served
 * Created by: gerolf
 * Date: 07/08/13
 * Time: 15:20
*/


class GoogleMapImages @Inject()(config: Configuration, c: ControllerComponents, langs:Langs, system:ActorSystem, arp:AutoRefreshingProperties) extends AbstractController(c) with I18nSupport {

  val IMAGEPATH = StringValue(arp.getProperty("PATH_MAP_IMAGES")).getOrElse("public/images/maps") + "/"
  val timeout = IntValue(arp.getProperty("GOOGLEMAP_TIMEOUT")).getOrElse(5000)
  val maxage = IntValue(arp.getProperty("GOOGLEMAP_MAXAGE")).getOrElse(60) *60*1000

  // start the purge job
  setupTimer

  def imagesReady(ecallId:String) = {
    // what files?
    val name = "ecall"+ecallId
    val filenames = Seq("_low","_mid","_high") map (suffix => IMAGEPATH+name+suffix+".png")
    Action.async {
      Future{waitTillFilesReady(filenames,timeout)}.map { result =>
        Ok("Images ready")
      }
    }
  }

  def getMap(path:String) = Action {
    if (new java.io.File(IMAGEPATH+path).exists) Ok.sendFile(new java.io.File(IMAGEPATH+path)).withHeaders((CACHE_CONTROL, "no-cache"))
    else NotFound
  }

  // this method should block until the files are there or the timeout occurs
  def waitTillFilesReady(filenames:Seq[String], timeout:Long) = {
    Logger.info("waiting (max %s milliseconds) for images to become ready....".format(timeout))
    var ok=false
    val start = DateTime.now
    while (!ok) {
       ok =  filenames.forall(filename => {
          new File(filename).exists && new File(filename).length() > 0
       })
       Thread.sleep(100)
       if (DateTime.now.minus(start.getMillis).getMillis > timeout ) {
         Logger.warn("timed out while waiting for Google Images!")
         ok = true
       }
    }
    // if we return immediatelly, some images give 404
    //Thread.sleep(350)
    Logger.info("all Google map images are ready!")
  }

  def setupTimer = {
    Logger.info("Starting periodic Purge job")
    //schedule (purge) every 5 mins
    ActorSystem().scheduler.schedule(0.seconds, 1.minutes, new Runnable {
      def run() {
        purge
      }
    })
  }

  def purge = {
    val files = new java.io.File(IMAGEPATH).listFiles()
    if (files!=null) {
      for (file <- files.toIterator if (file.isFile && (DateTime.now.getMillis - maxage) > file.lastModified)) {
        Logger.info("deleting file %s with age which is older than %s minutes".format(file.getName, file.lastModified, maxage / 1000 / 60))
        file.delete()
      }
    }
  }
}

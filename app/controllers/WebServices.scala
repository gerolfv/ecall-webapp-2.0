package controllers

import akka.actor.ActorSystem
import com.google.inject.Inject
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import org.slf4s.Logging
import play.api.Configuration
import play.api.i18n._
import play.api.libs.json.JsValue
import play.api.libs.json.Json._
import play.api.mvc._
import services.{JsonDetail, MSD, _}
import util._

import scala.collection.mutable

/**
 * Created by: gerolf
 * Date: 08/08/13
 * Time: 14:42
 */
class WebServices @Inject() (config: Configuration, c: ControllerComponents, langs:Langs, system:ActorSystem, arp:AutoRefreshingProperties, eds:EcallDetailService, icds:IncidentService) extends AbstractController(c) with I18nSupport with Logging {

  val auditlog = LoggerFactory.getLogger("audit")
  val lang = langs.availables.head
  implicit val messages: Messages = MessagesImpl(lang, messagesApi)
//  implicit val messagesProvider:MessagesProvider = MessagesImpl(lang,messagesApi)


  def incidents = Action {implicit Request =>
      // retrieve the last x minutes
      val time = IntValue(arp.getProperty("LIST_OLDEST_INCIDENT")) getOrElse(60)   // in minutes
      // get the data
      // timestamp in MSD is in seconds!
      val maxincidentage = (DateTime.now.getMillis/1000)-(time*60)
      try {
        val incidents = icds.recentEcalls(maxincidentage)
          val rv = incidents match {
            case Some(incs) => incs.foldLeft(mutable.ListBuffer.empty[JsValue]){((buffer,inc) => buffer += incidentBlock(inc))}
            case None => List[JsValue](toJson("warning" -> "No new incidents in the last "+time.toString+" minutes"))
        }
        Ok(toJson(rv))
      }catch {
        case t:Throwable => log.error("Error while retrieving incident list", t)
          Ok(toJson(Messages("general.error")))
      }
    }

  def incidentSearch = Action {implicit Request =>
    val term = (Request.getQueryString("query").getOrElse(""))
    auditlog.info("{WS} [ecall ?] - %s searches for ecalls with searchterm %s" format(Request.remoteAddress ,term))
    try {
      val incidents = icds.searchForEcalls(term)
      val rv = incidents match {
        case Some(incs) => incs.foldLeft(mutable.ListBuffer.empty[JsValue]){((buffer,inc) => buffer += incidentBlock(inc))}
        case None => List[JsValue](toJson("Warning" -> "No incidents found for the term "+term+" minutes"))
      }
      Ok(toJson(rv))
    }catch {
      case t:Throwable => log.error("Error while retrieving incident list", t)
        Ok(toJson(Messages("general.error")))
    }
  }

  def incidentDetail(id:String)= Action {implicit Request =>
    val clientIp = Request.remoteAddress
    auditlog.info("{WS} [ecall %s] - %s requests ecall detail %s" format(id,clientIp,id))
    try {
      val msd:Option[MSD] = eds.getMsd(id.toInt)
      msd match {
        case Some(m) => auditlog.info("{WS} [ecall %s] - %s retrieves detail for ecall %s" format(id, clientIp, m.uuid))
        case _ =>
      }
      val additional:Option[Additional] = eds.getAdditionalInfo(id.toInt)
      val raw:Option[String] =  eds.getRaw(id.toInt)
      val rv = msd match {
        case Some(m) => JsonDetail(m,additional,raw)(lang,messagesApi).content
        case None =>  toJson(Messages("detail.notAvailable",id))
      }
      Ok(toJson(rv))
    } catch {
      case t:Throwable => log.error("Error while retrieving incident detail for ecall id:%s".format(id), t)
        Ok(toJson(Messages("general.error")))
    }
  }


  def incidentBlock (inc:Incident) = {
    toJson(
      Map(
        "id" -> toJson(inc.id.toString),
        "timestamp" -> toJson(inc.date.toString("dd/MM/yyyy")+":"+inc.date.toString("HH:mm:ss")),
        "uuid" -> toJson(inc.uuid),
        "sid" -> toJson(inc.sid),
        "type" -> toJson(inc.reqtype),
        "tpsp" -> toJson(inc.tpsp)
      )
    )
  }
}

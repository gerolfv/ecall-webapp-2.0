package controllers

import java.io.File

import javax.inject._
import org.joda.time.DateTime
import org.slf4s.{LoggerFactory, Logging}
import play.api.i18n._
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import play.api.routing.JavaScriptReverseRouter
import play.api.{Configuration, Logger}
import services.{Detail, EcallDetailService, Incident, IncidentService}
import util._

import scala.collection.mutable


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class Application @Inject() (cc: ControllerComponents, config:Configuration, gmd:GoogleMapDownloader, eds: EcallDetailService, icds: IncidentService)
  extends AbstractController(cc) with I18nSupport with Logging {

  val auditlog = LoggerFactory.getLogger("audit")
  val versionString = config.get[String]("versionstring")
  val ec = defaultExecutionContext


  def javascriptRoutes = Action { implicit request =>
    Ok(
      JavaScriptReverseRouter("jsRoutes")(
        routes.javascript.Application.resendMSD,
        routes.javascript.Application.callback,
        routes.javascript.Application.incidentList,
        routes.javascript.Application.incidentSearch,
        routes.javascript.Application.incidentDetail,
        routes.javascript.Application.changeLocale,
        routes.javascript.GoogleMapImages.imagesReady
      )
    ).as("text/javascript")
  }


  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    implicit val lang = request.lang(messagesApi)
    Ok(views.html.index(versionString))
  }

  // this function shows the interface, but preload the search with a specific ecall
  def showIncident(ecallId:String) = Action {implicit Request =>
    implicit val lang = Request.lang(messagesApi)
    Ok(views.html.index(versionString,ecallId))//(lang,messagesProvider))
  }

  def resendMSD = Action {implicit Request =>
    val ecallId = Request.getQueryString("ecallId").map { id =>
      log.info(s"MSD resend asked for ecall ${id}")
      Ok("msd update has been asked")
    }
    BadRequest("Should have an ecallId query parameter to do this")
  }

  def callback = Action {implicit Request =>
    val ecallId = Request.getQueryString("ecallId").map { id =>
      val phone = Request.getQueryString("phone").map {
        p =>
          log.info(s"callback requested for ecall ${id} to phone ${p}")
          Ok("car will phone you back")
      }
    }
    BadRequest("Should have an ecallId and phone query parameter to do this")
  }

  // called asynchronously
  def incidentList = Action {implicit Request =>
    implicit val lang = Request.lang(messagesApi)
    // retrieve the last x minutes (from configfile
    val time = IntValue(config.get[String]("LIST_OLDEST_INCIDENT")) getOrElse(60)   // in minutes
  // MSD timestamp in database is in milliseconds!
  val maxincidentage = (DateTime.now.getMillis)-(time*60*1000)
    // get the data
    try {
      val incidents = icds.recentEcalls(maxincidentage)
      val html = incidents match {
        case Some(incs) => incs.foldLeft(mutable.StringBuilder.newBuilder){((buffer,inc) => buffer.append(incidentBlock(inc)))}
        case None => "<strong>No new incidents in the last "+time+" minutes</strong>"
      }
      Ok(html.toString).withHeaders(("Cache-Control", "no-cache"))
    }catch {
      case t:Throwable => Logger.error("Error while retrieving incident list", t)
        Ok(Messages("general.error"))//(messagesProvider))
    }
  }

  // called asynchronously
  def incidentSearch = Action {implicit Request =>
    implicit val lang = Request.lang(messagesApi)
    val term = Request.getQueryString("query")getOrElse("")
    auditlog.info("[ecall ?] - %s searches for ecalls with searchterm %s" format(Request.remoteAddress ,term))
    try {
      val incidents = icds.searchForEcalls(term)
      val html = incidents match {
        case Some(incs) => incs.foldLeft(mutable.StringBuilder.newBuilder){((buffer,inc) => buffer.append(incidentBlock(inc)))}
        case None => "<strong>No results for query '"+term+"'</strong>"
      }
      Ok(html.toString).withHeaders(("Cache-Control", "no-cache"))
    } catch {
      case t:Throwable => Logger.error("Error while retrieving incident list searching %s".format(term), t)
        Ok(Messages("general.error"))//(messagesProvider))
    }
  }

  // called asynchronously
  def incidentDetail(id:String)= Action { implicit request =>
    implicit val lang = request.lang(messagesApi)
    val clientIp = request.remoteAddress
    Logger.info("client %s asks detail %s" format(clientIp,id))
    auditlog.info("[ecall %s] - %s requests ecall detail %s" format(id,clientIp,id))
    try {
      Logger.info("getting MSD %s from service" format id)
      val msd = eds.getMsd(id.toInt)
      Logger.info("got MSD %s from service" format id)
      msd match {
        case Some(m) => auditlog.info("{WS} [ecall %s] - %s retrieves detail for ecall %s" format(id, clientIp, m.uuid))
        case _ => Logger.warn("No MSD found for Ecall with id " format id)
      }
      Logger.info("getting additional %s from service" format id)
      val additional = eds.getAdditionalInfo(id.toInt)
      Logger.info("getting raw %s from service" format id)
      val raw = eds.getRaw(id.toInt)
      val html:String = msd match {
        case Some(m) => {
          Logger.info("generating maps")
          gmd.generateMapsAsync("ecall"+id,LocationInfo(m.positions))
          Logger.info("building detail")
          Detail(m,additional,raw)(lang,messagesApi).content.foldLeft("")((b,a) => b ++ a.toString)
        }
        case None =>  Messages("detail.notAvailable",id)
      }
      Logger.info("returning page")
      Ok(html).withHeaders(("Cache-Control", "no-cache"))
    } catch {
      case t:Throwable => {
        Logger.error("Error while retrieving incident detail for ecall id:%s".format(id), t)
        Ok(Messages("general.error"))
      }
    }
  }

  // called asynchronously
  def changeLocale(locale:String) = Action { implicit request =>
    val referrer = request.headers.get("referer").getOrElse("/")
    implicit val lang = Lang(locale)
    Redirect(referrer).withLang(lang)
    //Ok.withLang(Lang(locale))//(messagesApi)
    //Ok(views.html.index())
  }

  // called asynchronously
  def showHelp = Action { implicit request =>
    implicit val lang = request.lang
    val filename = "public/documents/manual_%s.pdf" format lang.code
    Ok.sendFile(new File(filename))(ec,fileMimeTypes)
  }

  def resendMSD(id:String) = Action {implicit request =>
    val oldmsd = eds.getMsd(id.toInt)

    Ok(Messages("extended.msdupdated"))
  }

  def incidentBlock(inc:Incident)(implicit lang:Lang) = {
    implicit val messagesProvider: MessagesProvider = {
      MessagesImpl(lang, messagesApi)
    }
    <div class="message-box" data-block-id={inc.id.toString}>
      <dl>
        <dt>{Messages("ecall.received")}</dt>
        <dd>{inc.date.toString("dd/MM/yyyy")} &#8226; {inc.date.toString("HH:mm:ss")}</dd>

        <dt>UID</dt>
        <dd>{inc.uuid}</dd>

        <dt>SID</dt>
        <dd>{inc.sid}</dd>

        <dt>Telephone</dt>
        <dd>{inc.telephone}</dd>

        <dt>Type</dt>
        <dd>{inc.reqtype}</dd>

        <dt>TPSP</dt>
        <dd>{inc.tpsp}</dd>

      </dl>
    </div>
  }

}

package util

import java.util.Properties

import akka.actor.ActorSystem
import com.google.inject.Inject
import com.typesafe.config.Config
import org.slf4s.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


/**
  * Created by: gerolf
  * Date: 05/07/12
  * Time: 13:41
  */

object IntValue {
  def unapply(x: Int) = x.toString
  def apply(x: AnyRef): Option[Int] = x match {
    case s:String => try { Some(s.toInt) } catch { case _: Throwable => None }
    case _ => None
  }
}

object StringValue {
  def unapply(x: String) = x.toString
  def apply(x: AnyRef): Option[String] = x match {
    case s:String => Some(s)
    case _ => None
  }
}


class AutoRefreshingProperties @Inject()(config:Config) extends Logging {


  val MAX_PREFIX_LENGHT = 4
  val APPPROPS_LOCATION = "settings.properties"
  val props = new Properties()

  //setupTimer

  def getProperty(name: String): String = {
    //if (props.isEmpty) loadFiles
    //props.getProperty(name)
    config.getString(name)

  }

  def loadFiles = {
    try {
      //props.load(this.getClass().getResourceAsStream(APPPROPS_LOCATION))
    } catch {
      case e: Exception => log.error("problem while reading settings file. Make sure conf/settings.properties is in the classpath ")
    }
  }

  def setupTimer = {
    //schedule (loadFiles) every 5 mins
    ActorSystem().scheduler.schedule(0.seconds, 5.minutes, new Runnable {
      def run() {
        loadFiles
      }
    })
  }
}

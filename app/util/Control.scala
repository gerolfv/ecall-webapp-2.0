package util

import collection.mutable.ListBuffer
import java.util.concurrent.TimeoutException
import play.api.Logger

/**
  * Created by: gerolf
  * Date: 05/10/12
  * Time: 22:41
  */
object Control {

  def using[A <: {def close()},B](p: A)(f: A => B) = {
    try {
      f(p)
    } finally {
      try {
        p.close()
      } catch {
        case  t: Throwable => Logger.error("Error closing resource %s " format p, t)
      }
    }
  }

  def allResults[T](b: => Boolean)(f: => T): List[T] = {
    val rv = new ListBuffer[T]
    while(b) rv += f
    rv.toList
  }

  def singleResult[T](b: => Boolean)(f: => Option[T]): Option[T] = {
    if(b) (f) else None
  }

  def NoneOnException [B <: Any](f: => Option[B]): Option[B] = {
    try {
      f  //execute method
    } catch {
      case e: Throwable => {
        Logger.warn("exception while performing task inside NoneOnException ",e)
        None
      }
    }
  }

  def withTiming[B<:Any](f: => B): (B, Long) = {
    val start = System.currentTimeMillis
    val b = f
    val stop = System.currentTimeMillis
    (b,stop-start)
  }

  def withTimingOrTimeout[B<:Any](f: => B): (Option[B], Long) = {
    val start = System.currentTimeMillis()
    try {
      val res = withTiming(f)
      (Some(res._1),res._2)
    } catch {
      case toe: TimeoutException => {
        val stop = System.currentTimeMillis()
        Pair(None,stop-start)
      }
    }
  }
}

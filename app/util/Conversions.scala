package util

import services.Position

/**
  * Created by: gerolf
  * Date: 10/09/13
  * Time: 14:50
  */
object Conversions {
  def milliArcSecondsToDegrees(milliarcsecs:Double) = {
    var milliarcseconds = milliarcsecs/* input and output */
    val degrees = Math.floor(milliarcseconds / 3600000)
    milliarcseconds -= degrees*3600000
    val minutes = Math.floor(milliarcseconds / 60000)
    milliarcseconds -= minutes*60000
    val seconds = Math.floor(milliarcseconds) / 1000
    milliarcseconds -= seconds*1000
    "%02.0f".format(degrees)+":"+"%02.0f".format(minutes)+":"+"%02.4f".format(seconds+milliarcseconds).trim
  }

  def milliArcSecondsToDecimals(pos:Position):String = {
    ""+milliArcSecondsToDecimals(pos.latitude)+","+milliArcSecondsToDecimals(pos.longitude)
  }

  def milliArcSecondsToDecimals(milliarcsecs:Double):String = {
    var milliarcseconds = milliarcsecs/* input and output */
    val degrees = Math.floor(milliarcseconds / 3600000)
    milliarcseconds -= degrees*3600000
    val minutes = Math.floor(milliarcseconds / 60000)
    milliarcseconds -= minutes*60000
    val seconds = milliarcseconds
    //"%2.7f".format(degrees+(minutes/60)+(seconds/3600))
    "%2.7f".format(milliarcsecs / 3600000).trim
  }
}

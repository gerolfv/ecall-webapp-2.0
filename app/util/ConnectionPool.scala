package util

import com.jolbox.bonecp.{BoneCP, BoneCPConfig}
import java.sql.Connection

import com.google.inject.Inject
import com.typesafe.config.Config
import play.api.Play


/**
  * Created by: gerolf
  * Date: 06/10/12
  * Time: 14:18
  */
class ConnectionPool  @Inject()(config:Config) {

  Class.forName(config.getString("DB_DRIVER"))

  def createConfig(boneconfig:BoneCPConfig) = {
    boneconfig.setJdbcUrl(config.getString("DB_URL"))
    boneconfig.setUsername(config.getString("DB_USERNAME"))
    boneconfig.setPassword(config.getString("DB_PASSWORD"))
    boneconfig.setPartitionCount(IntValue(config.getString("DB_PARTITIONCOUNT")) getOrElse 1)
    boneconfig.setMinConnectionsPerPartition(IntValue(config.getString("DB_MINCONS_PER_PARTITION")) getOrElse 1)
    boneconfig.setMaxConnectionsPerPartition(IntValue(config.getString("DB_MAXCONS_PER_PARTITION")) getOrElse 5)
    val conage:Int = IntValue(config.getString("DB_MAXCONAGE")) getOrElse 60
    boneconfig.setMaxConnectionAgeInSeconds(conage.toLong)
    boneconfig
  }

  lazy val pool = new BoneCP(createConfig(new BoneCPConfig))


  def getConnection: Connection = {
    pool.getConnection
  }
}

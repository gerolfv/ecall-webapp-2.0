package util

import com.google.inject.Inject
import com.typesafe.config.Config

import scala.concurrent.{ExecutionContext, Future}

//import akka.http.scaladsl.model.{HttpRequest, StatusCode}
import java.io.{BufferedOutputStream, File, FileOutputStream}
import java.net.URL

import play.api.Logger
import services.Position

import scala.sys.process._

//import akka.http.scaladsl.model._
import util.Conversions._

/**
  * This actor is pooled
  * Created by: gerolf
  * Date: 11/07/13
  * Time: 15:03
  */

case class LocationInfo(locations:Seq[Position])  {
  def lastPoint = locations(0)
}

class GoogleMapDownloader @Inject()(config:Config, implicit val ec:ExecutionContext) {

  val GOOGLEMAPS_URL = StringValue(config.getString("GOOGLEMAPS_URL")).getOrElse("https://maps.googleapis.com")
  val APIKEY = StringValue(config.getString("GOOGLEMAPS_APIKEY")).getOrElse("AIzaSyC47frezx-Pa3ywDxzoeNhtqhmtgt3va2M")
  val IMAGEPATH = StringValue(config.getString("PATH_MAP_IMAGES")).getOrElse("public/images/maps") + "/"
  val SIZE =  StringValue(config.getString("MAP_SIZE")).getOrElse("750x350")
  val CHECKFOREXSITINGMAPS = StringValue(config.getString("CHECKFOREXSITINGMAPS")).getOrElse("true").toBoolean
  val zoom_low = StringValue(config.getString("ZOOM_LOW")).getOrElse("12")
  val zoom_mid = StringValue(config.getString("ZOOM_MID")).getOrElse("14")
  val zoom_high = StringValue(config.getString("ZOOM_HIGH")).getOrElse("17")
  val maptype= "roadmap"

  def generateMapsAsync(uuid:String,loclist:LocationInfo): Unit = {
    Future {
      generateMaps(uuid,loclist)
    }
  }

  def generateMaps(uuid:String,loclist:LocationInfo) = {
    // first check if the folder exists. If not -> create it
    if (! new File(IMAGEPATH).exists || ! new File(IMAGEPATH).isDirectory) {
      new File(IMAGEPATH).delete()
      new File(IMAGEPATH).mkdir
    }
    // check if these images already exist or whether it has size 0
    val condition = {
      if (CHECKFOREXSITINGMAPS) (!new File(IMAGEPATH+uuid+"_low.png").exists || new File(IMAGEPATH+uuid+"_low.png").length()==0)
      else true
    }
    if (condition) {
      // construct the urls
      val url_low = generateMapUrl(loclist,maptype,zoom_low,SIZE)
      val url_mid = generateMapUrl(loclist,maptype,zoom_mid,SIZE)
      val url_detail = generateMapUrl(loclist,maptype,zoom_high,SIZE)
      //val url_streetview = generateStreetViewUrl(loclist.locations(0),SIZE,loclist.locations(0).heading.toString)
      DownloadImage(url_low,IMAGEPATH+uuid+"_low.png")
      DownloadImage(url_mid,IMAGEPATH+uuid+"_mid.png")
      DownloadImage(url_detail,IMAGEPATH+uuid+"_high.png")
      //DownloadImage(url_streetview,IMAGEPATH+uuid+"_streetview.png")
    }
    else {
      Logger.info("Images for Ecall %s already exist".format(uuid))
    }
  }

  def generateStreetViewUrl(location:Position,size:String,heading:String) = {
    val builder = StringBuilder.newBuilder
    builder ++= GOOGLEMAPS_URL+"/maps/api/streetview?"
    builder ++= "location="+location
    builder ++= "&size="+size
    builder ++= "&heading="+heading
    builder ++= "&sensor=true"
    builder.toString
  }

  def generateMapUrl(loclist:LocationInfo,maptype:String,zoomlevel:String,size:String) = {
    if (loclist.locations.size ==0) {
      Logger.error("No GPS locations for this ecall. Maps will not be generated")
    }
    require (loclist.locations.size > 0)
    // the locations are given in ISO 6709 arcmillis. We need to convert to decimal degrees
    val sep = "%7C"
    val builder = StringBuilder.newBuilder
    builder ++= GOOGLEMAPS_URL+"/maps/api/staticmap?"
    // center = last position
    builder ++= "center="+milliArcSecondsToDecimals(loclist.locations(0))
    // roadmap
    builder ++= "&maptype="+maptype
    // zoomlevel 13
    builder ++= "&zoom="+zoomlevel
    // size = 600x400
    builder ++= "&size="+size
    // markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green
    // %7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Ccolor:red%7Clabel:C%7C40.718217,-73.998284&sensor=false
    // the markers:
    builder ++= "&markers=color:red"+sep+"label:0"+sep+milliArcSecondsToDecimals(loclist.locations(0))
    0 to loclist.locations.size-1 foreach (i => {
      builder ++= "&markers=color:blue"+sep+"label:"+(i)+sep+milliArcSecondsToDecimals(loclist.locations(i))
    })
    // the positions are measured by a GPS sensor
    builder ++= "&sensor=true"
    builder ++= s"&key=${APIKEY}"
    builder.toString
  }


  def DownloadImage(theurl:String,filename:String) {
    // filename has the relative path
    val file = new java.io.File(".").getAbsolutePath()+"/"+filename
    val f = new java.io.File(file)
    if (!f.exists()) {
      f.createNewFile()
    }
    val out = new BufferedOutputStream(new FileOutputStream(f,false))
    try {
      Logger.info("trying to get google maps image from url %s " format(theurl))
      (new URL(theurl) #> f !!)
      //  out.write(im)
      Logger.info("download of file %s complete" format filename)
    } catch {
      case t:Throwable => Logger.error("could not download and store Google Map", t)
        if (out!=null) out.close()
        f.delete // image failed to download - file is empty
    }
  }
}


/*
case class DownloadImage(url:String,filename:String)
case class DownloadComplete(filename:String)

class FileDownloader extends Actor {

  def receive= {
    case DownloadImage(url:String,filename:String) => {
      val file = new java.io.File(".").getAbsolutePath()+"/"+filename
      //val file = "/Users/gerolf/consult/e-call/code/ECallWebapp/public/images/maps/ecall66_streetview.png"
      val f = new java.io.File(file)
      if (!f.exists()) {
        f.createNewFile()
      }
      val out = new BufferedOutputStream(new FileOutputStream(f,false))
      try {
        implicit val system = context.system
        implicit val timeout = akka.util.Timeout(2000, TimeUnit.MILLISECONDS)
        val response: Future[HttpResponse] = (IO(Http) ? HttpRequest(GET, Uri(s"$url"))).mapTo[HttpResponse]
        val result:HttpResponse = Await.result(response, timeout.duration)
        val entity = result.entity
        out.write(entity.buffer)
        sender ! DownloadComplete(filename)
      } catch {
        case t:Throwable => Logger.error("could not download and store Google Map", t)
      } finally {
        if (out!=null) out.close()
      }
    }
  }
}
*/
